﻿using FluentAssertions;
using ScoreCardRebuild;
using Xunit;

namespace Tests
{
    public class ScoreCardFacts
    {
        [Fact]
        public void OneRoll()
        {
            //Arrange / Act
            var sut = new ScoreCard("1");

            //Assert
            sut.GameScore().Should().Be(1);
        }

        [Fact]
        public void TwoRolls()
        {
            //Arrange / Act
            var sut = new ScoreCard("1,1");

            //Assert
            sut.GameScore().Should().Be(2);
        }

        [Fact]
        public void Strike()
        {
            //Arrange / Act
            var sut = new ScoreCard("X");

            //Assert
            sut.GameScore().Should().Be(10);
        }

        [Fact]
        public void TwoStrikes()
        {
            //Arrange / Act
            var sut = new ScoreCard("X|X");

            //Assert
            sut.GameScore().Should().Be(30);
        }

        [Fact]
        public void TwoSpares()
        {
            //Arrange / Act
            var sut = new ScoreCard("1,/|1,/");

            //Assert
            sut.GameScore().Should().Be(21);
        }

        [Fact]
        public void LastFrameSpareStrike()
        {
            //Arrange / Act
            var sut = new ScoreCard("1,2|1,2|1,2|1,2|1,2|1,2|1,2|1,2|1,2|2,8,X");

            //Assert
            sut.GameScore().Should().Be(47);
        }

        [Fact]
        public void LastFrameStrike()
        {
            //Arrange / Act
            var sut = new ScoreCard("1,2|1,2|1,2|1,2|1,2|1,2|1,2|1,2|1,2|X,1,1");

            //Assert
            sut.GameScore().Should().Be(39);
        }

        [Fact]
        public void LastFrameStrikeSpareAltertnativeNotation()
        {
            //Arrange / Act
            var sut = new ScoreCard("1,2|1,2|1,2|1,2|1,2|1,2|1,2|1,2|1,2|X,2,/");

            //Assert
            sut.GameScore().Should().Be(47);
        }

        [Fact]
        public void RandomSpare()
        {
            //Arrange / Act
            var sut = new ScoreCard("1,2|1,2|1,/|1,2|1,2|1,2|1,2|1,2|1,2|1,2");

            //Assert
            sut.GameScore().Should().Be(38);
        }

        [Fact]
        public void RandomStrike()
        {
            //Arrange / Act
            var sut = new ScoreCard("1,2|1,2|X|1,2|1,2|1,2|1,2|1,2|1,2|1,2");

            //Assert
            sut.GameScore().Should().Be(40);
        }

        [Fact]
        public void Bonusless()
        {
            //Arrange / Act
            var sut = new ScoreCard("1,2|3,4|5,1|2,3|4,5|1,2|3,4|5,1|2,3|4,5");

            //Assert
            sut.GameScore().Should().Be(60);
        }

        [Fact]
        public void Heartbreak()
        {
            //Arrange / Act
            var sut = new ScoreCard("9|9|9|9|9|9|9|9|9|9");

            //Assert
            sut.GameScore().Should().Be(90);
        }

        [Fact]
        public void AllSpares()
        {
            //Arrange / Act
            var sut = new ScoreCard("5,/|5,/|5,/|5,/|5,/|5,/|5,/|5,/|5,/|5,/,5");

            //Assert
            sut.GameScore().Should().Be(150);
        }

        [Fact]
        public void CleanSheet()
        {
            //Arrange / Act
            var sut = new ScoreCard("5,/|5,/|5,/|5,/|5,/|5,/|5,/|5,/|5,/|5,/,5");

            //Assert
            sut.GameScore().Should().Be(150);
        }

        [Fact]
        public void Dutch200()
        {
            //Arrange / Act
            var sut = new ScoreCard("3,/|X|3,/|X|3,/|X|3,/|X|3,/|X,3,/");

            //Assert
            sut.GameScore().Should().Be(200);
        }

        [Fact]
        public void NoStrikes()
        {
            //Arrange / Act
            var sut = new ScoreCard("1,2|8,1|3,2|3,2|3,4|9,0|3,4|1,3|4,3|4,3");

            //Assert
            sut.GameScore().Should().Be(63);
        }
    }
}
