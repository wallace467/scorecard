﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCardRebuild
{
    public class Bowling
    {
        public void Play()
        {
            Console.WriteLine("Please enter scores in the format of 1,2|3,4|5,1|2,3|4,5|1,2|3,4|5,1|2,3|4,5 (X denotes strike, / denotes a spare.");
            var scores = Console.ReadLine().ToLower();

            var scoreCard = new ScoreCard(scores);

            Console.WriteLine(scoreCard.GameScore());

            Console.ReadKey();
        }


    }
}
