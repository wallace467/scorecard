Bowling Score Card Testing

We have created a new bowling score-card program. It is a revolutionary new idea that will change the way people use their computers for bowling score-card related calculations.
Unfortunately there seem to be a few bugs in it but as we are so busy writing marketing plans and talking to top industry bowling sponsors that we don’t have time to figure out what they are.
The developer swears their program is perfect, there are no bugs and it is fully covered by unit tests.
This is where you come in!
Specification

The program is a .NET program that runs as a console application.
The program accepts input from a user in a specific format and then calculates the final score from it and prints that to the screen.
It must calculate the final score based upon the ‘Rules of Bowling’ - http://www.rulesofbowling.com/How-to-Score-the-game-Bowling-using-a-Score-Card.php 
There are some unit tests already created for this programs.

Can you:
•	Show an example of a test plan you might create for this application.
•	Identify any problem or missing scenarios and write any supplemental unit tests that may be needed.
•	Create an integration test for the application.
•	Suggest any other improvements we could make to the application or the way it works to make it more testable / readable or supportable going forward.
